<H1>Docker für Synology DS218 (arm)</H1>

Das fertige ipk Paket kann <a href="https://gitlab.com/derek.ziegler/synodocker/-/raw/master/syno-docker-19.03.09.ipk?inline=false">hier </a>  direkt herunter geladen werden.
Installation mit "ipkg install syno-docker-19.03.09.ipk"

Grundlage ist statisches Docker für aarch64 Version 19.03.09.

Alternativ kann dieses Repository mit git clone auf die Synology heruntergeladen werden.
Mit "create.sh" wird ein neues ipk generiert.

Voraussetungen:
curl, tar und ar

<I>Die Funktionalität dieser Dockerversion ist sehr eingeschränkt. Zahlreiche Funktionen stehen nicht zur Verfügung!</I><br>

<b>Ausführen eines Containers z.B. mit "docker  run --rm -it -v /dev:/dev --net=host hello-world</b><br>
Weitere Infos zu der Installation und den Einschränkungen  unter https://www.ziegler-eu.de
