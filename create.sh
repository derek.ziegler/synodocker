#!/bin/bash
#set -x
CURL_URL=https://download.docker.com/linux/static/stable/aarch64/docker-19.03.9.tgz

which curl 
if [ $? != 0 ]; then
	echo 'Curl nich gefunden. Bitte installieren! Exit.....'
	exit -1
fi
echo 'Curl vorhanden. Weiter...'
echo 

which ar 
if [ $? != 0 ]; then
	echo 'ar nich gefunden. Bitte ipkg install bintools ausfuehren! Exit.....'
	exit -1
fi
echo 'ar vorhanden. Weiter...'
echo 

echo 'Lade Static Binaries docker herunter...'
mkdir -p docker_tmp
curl $CURL_URL -o ./docker_tmp/docker.tgz
echo
echo 'Extrahiere...'
( cd docker_tmp; tar xzf docker.tgz )

mv docker_tmp/docker/* opt/bin

echo 'Erstelle Datafile...'
tar czvf ./data.tar.gz opt
tar czvf ./control.tar.gz control preinst prerm postinst

echo 'Erzeuge Paket'
mkdir -p pack
mv data.tar.gz control.tar.gz pack
cp -p control debian-binary pack

cd pack;ar r ../syno-docker-19.03.09.ipk control control.tar.gz data.tar.gz debian-binary ; cd ..
rm -rf pack docker_tmp ./opt/bin/*


echo 'Fertig! Install mit "ipkg install syno-docker-19.03.09.ipk"'


